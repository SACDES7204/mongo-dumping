!/bin/bash
#set -e
echo "Setting up configuration">>output/log/dump.log
#Configuration Starts Here
HOST=
DBNAME=
USER='core'
PASS=


echo "Finding backup date">>output/log/dump.log
date_now=`date +%s%3N`
date_1day_ago=$[date_now - 1 * 24 * 60 * 60 * 1000]
echo "Done">>output/log/dump.log



echo $date_now
echo $date_1day_ago

#Configuration Ends Here
echo "Configuration Success">>output/log/dump.log

echo "Creating Backup">>output/log/dump.log

#echo "Locking the Sync with Replicaset">>output/log/dump.log
#mongo admin --eval "db.auth('siteUserAdmin','Cd8c9udf8GUY7bBBnyibiunHN7e7hh8c7hc87ehc7e8');printjson(db.fsyncLock())"


echo "Waiting for syncLock to get activated, Please wait....">>output/log/dump.log
sleep 5

echo "Backing up now...."
sleep 2



for i in `cat collections.txt` ;
        do mongodump --username $USER --password $PASS --db $DBNAME --collection $i --query "{ \"updatedAt\": {\"\$gte\": {\"\$date\":$date_1day_ago }}}" --oplog --out --oplog output/videousdump/ ;done

#Data Dumping Ends Here
TIME=`date +%s%3N`
echo "Data Backup was sucess on  $TIME">>output/log/dump.log
echo "Backup Done ">>output/log/dump.log

#echo "Unlocking the Sync with Replicaset">>output/log/dump.log
#mongo admin --eval "db.auth('siteUserAdmin','Cd8c9udf8GUY7bBBnyibiunHN7e7hh8c7hc87ehc7e8');printjson(db.fsyncUnlock())"
echo "done">>output/log/dump.log

#Converting to Tar file
echo "compressing the dump">>output/log/dump.log
tar -czvf dump.tar.gz output/videousdump
chmod 777 dump.tar.gz
#Uploading to Google Cloud Storage Bucket
echo "Initializing connection with google cloud storage">>output/log/dump.log
gsutil cp dump.tar.gz gs://videous-dump



#Removing all backup files from local
echo "Removing all backup files from local, please wait...."
sleep 2
rm -rf dump.tar.gz
rm -rf output/videousdump/
#rm -rf output/log/dump.log
echo "cleaning finished"


sleep 1
echo "Good Bye"        
        